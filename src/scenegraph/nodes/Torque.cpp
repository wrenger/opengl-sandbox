#include "Torque.hpp"

namespace Sandbox {

Torque::Torque(const glm::vec3 &force) : Spatial(), force(force) {}

void Torque::Update(float delta, bool updated, const glm::mat4 &transform) {
    glm::vec3 rotation = this->GetTransform().Rotation();
    rotation += force * delta;
    this->GetTransformMut().SetRotation(rotation);

    Spatial::Update(delta, true, transform);
}

} // namespace Sandbox
