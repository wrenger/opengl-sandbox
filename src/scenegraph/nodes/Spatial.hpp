#pragma once

#include <glm/glm.hpp>

#include "scenegraph/Node.hpp"
#include "scenegraph/WorldTransform.hpp"

namespace Sandbox {

/**
 * @brief   Basic 3D game object.
 */
class Spatial : public Node {
  private:
    WorldTransform transform;
    bool updateTransform;

  public:
    /**
     * @brief Construct a new spatial node.
     *
     * @param position  Local position
     * @param rotation  Local rotation
     * @param scale     Local scale
     */
    Spatial(const glm::vec3 &position = glm::vec3(0),
            const glm::vec3 &rotation = glm::vec3(0),
            const glm::vec3 &scale = glm::vec3(1));

    Spatial(Spatial const &) = delete;
    void operator=(Spatial const &) = delete;

    /**
     * @brief Returns the nodes transformation
     */
    inline const WorldTransform &GetTransform() const { return transform; }

    /**
     * @brief Returns the transformation to be modified.
     */
    WorldTransform &GetTransformMut();

    /**
     * @brief Updates the transformation.
     */
    void SetTransform(const WorldTransform &transform);

    virtual void Update(float delta, bool updated = false,
                        const glm::mat4 &transform = glm::mat4(1.0f)) override;
};

} // namespace Sandbox
