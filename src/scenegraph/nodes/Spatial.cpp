#include "Spatial.hpp"

namespace Sandbox {

Spatial::Spatial(const glm::vec3 &position, const glm::vec3 &rotation,
                 const glm::vec3 &scale)
    : Node(), transform(position, rotation, scale), updateTransform(true) {}

void Spatial::SetTransform(const WorldTransform &transform) {
    this->transform = transform;
    updateTransform = true;
}

WorldTransform &Spatial::GetTransformMut() {
    updateTransform = true;
    return transform;
}

void Spatial::Update(float delta, bool updated, const glm::mat4 &transform) {
    updated = updated || updateTransform;
    if (updated) {
        this->transform.UpdateGlobal(transform);
    }
    Node::Update(delta, updated, this->transform.Model());
}

} // namespace Sandbox
