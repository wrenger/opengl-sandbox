#pragma once

#include <glm/glm.hpp>

#include "Spatial.hpp"

namespace Sandbox {

class Camera : public Spatial {
  private:
    glm::mat4 projection;
    glm::mat4 view;

  public:
    /**
     * @brief      Creates a new perspective camera.
     *
     * @param[in]  angle   The view angle or field of view.
     * @param[in]  aspect  The aspect ratio of the window
     * @param[in]  near    The near plane. All coordinates in front of this
     * plane are not drawn.
     * @param[in]  far     The far plane. All coordinates behind this plane are
     * not drawn.
     */
    Camera(float angle, float aspect, float nearPlane, float farPlane);

    Camera(Camera const &) = delete;
    void operator=(Camera const &) = delete;

    void Update(float delta, bool updated = false,
                const glm::mat4 &transform = glm::mat4(1.0f)) override;

    void OnEvent(Event &event) override;

    /**
     * @brief      Configures the perspective projection of the 3D world to the
     * 2D screen.
     *
     * @param[in]  angle   The view angle or field of view.
     * @param[in]  aspect  The aspect ratio of the window
     * @param[in]  near    The near plane. All coordinates in front of this
     * plane are not drawn.
     * @param[in]  far     The far plane. All coordinates behind this plane are
     * not drawn.
     */
    void SetProjection(float angle, float aspect, float nearPlane,
                       float farPlane);

    /**
     * @brief      Returns the projection matrix which defines the 3D projection
     *             to the 2D screen.
     *
     * @return     The projection matrix.
     */
    inline glm::mat4 const &Projection() const { return projection; }

    /**
     * @brief      Returns the view matrix which defines the location and the
     *             point of view of the camera.
     *
     * @return     The view matrix.
     */
    inline glm::mat4 const &View() const { return view; }
};

} // namespace Sandbox
