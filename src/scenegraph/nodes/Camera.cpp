#include "Camera.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

namespace Sandbox {

Camera::Camera(float angle, float aspect, float nearPlane, float farPlane)
    : Spatial() {
    SetProjection(angle, aspect, nearPlane, farPlane);
}

void Camera::Update(float delta, bool updated, const glm::mat4 &transform) {
    Spatial::Update(delta, updated, transform);
    view = Spatial::GetTransform().ViewMatrix();
}

void Camera::OnEvent(Event &event) {
    if (event.GetType() == EventType::WindowResize) {
        WindowResizeEvent &e = (WindowResizeEvent &)event;
        SetProjection(60.0f, ((float)e.GetWidth()) / e.GetHeight(), 0.01f,
                      1000.0f);
    }
    Node::OnEvent(event);
}

void Camera::SetProjection(float angle, float aspect, float nearPlane,
                           float farPlane) {
    projection =
        glm::perspective(glm::radians(angle), aspect, nearPlane, farPlane);
}

} // namespace Sandbox
