#pragma once

#include <glm/glm.hpp>

#include "Spatial.hpp"

namespace Sandbox {

/**
 * @brief   A node which rotates with a constant turning momentum.
 */
class Torque : public Spatial {
  private:
    glm::vec3 force;

  public:
    /**
     * @brief     Construct a new Torque node that rotates with a constant
     * speed.
     *
     * @param[in] force  The turning moment.
     */
    Torque(const glm::vec3 &force);

    Torque(Torque const &) = delete;
    void operator=(Torque const &) = delete;

    void Update(float delta, bool updated = false,
                const glm::mat4 &transform = glm::mat4(1.0f)) override;
};

} // namespace Sandbox
