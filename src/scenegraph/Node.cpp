
#include "Node.hpp"

#include <iostream>

namespace Sandbox {

Node::Node() : children(), updateChildren(false) {}

Node::~Node() {}

void Node::Update(float delta, bool updated, const glm::mat4 &transform) {
    for (auto const &child : children) {
        child->updateChildren = updateChildren;
        child->Update(delta, updated, transform);
    }
    updateChildren = false;
}

void Node::Render(float delta, const glm::mat4 &view,
                  const glm::mat4 &projection) const {
    for (auto const &child : children) {
        child->Render(delta, view, projection);
    }
}

void Node::OnEvent(Event &event) {
    for (auto const &child : children) {
        child->OnEvent(event);
    }
}

void Node::AddChild(std::shared_ptr<Node> obj) {
    children.emplace_back(std::move(obj));
    updateChildren = true;
}

void Node::RemoveChild(unsigned int index) {
    children.erase(children.begin() + index);
    updateChildren = true;
}

} // namespace Sandbox
