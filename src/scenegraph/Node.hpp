#pragma once

#include <glm/glm.hpp>
#include <memory>
#include <vector>

#include "Event.hpp"

namespace Sandbox {

/**
 * @brief      Node of the scene tree. Base class of all scene objects.
 */
class Node {
  private:
    std::vector<std::shared_ptr<Node>> children;
    bool updateChildren;

  public:
    /**
     * @brief      Creates a new node.
     */
    Node();

    /**
     * Destroys this node.
     */
    virtual ~Node();

    Node(Node const &) = delete;
    void operator=(Node const &) = delete;

    /**
     * @brief      Updates the model matrix of this node.
     *
     * @param[in]  delta      The time until the last render call in seconds.
     * @param[in]  updated    If the parent transform has changed.
     * @param[in]  transform  The transformation (model) matrix of the parent
     * object.
     */
    virtual void Update(float delta, bool updated = false,
                        const glm::mat4 &transform = glm::mat4(1.0f));

    /**
     * @brief      Renders this node and its children.
     *
     * @param[in]  delta      The time until the last render call in seconds.
     * @param[in]  view       The view matrix of the current camera.
     * @param[in]  projection The projection matrix of the current camera.
     */
    virtual void Render(float delta, const glm::mat4 &view,
                        const glm::mat4 &projection) const;

    /**
     * @brief Handles events.
     *
     * @param event[in] Event Class.
     */
    virtual void OnEvent(Event &event);

    /**
     * @brief      Returns the number of child nodes.
     */
    inline unsigned int ChildCount() const { return children.size(); }

    /**
     * @brief     If the children are updated during this frame.
     */
    inline bool ChildrenUpdated() const { return updateChildren; }

    /**
     * @brief      Adds the node to the list of child nodes.
     *
     * @param      obj   The new child object.
     */
    void AddChild(std::shared_ptr<Node> obj);

    /**
     * @brief      Returns the child at this index.
     *
     * @param[in]  i     The index of a child.
     */
    inline std::shared_ptr<Node> GetChild(unsigned int index) const {
        return children[index];
    }

    /**
     * @brief      Removes the node from the list of child nodes.
     *
     * @param[in]  i     The index of the node that should be removed.
     */
    void RemoveChild(unsigned int index);
};

} // namespace Sandbox
