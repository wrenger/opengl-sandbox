
#include "WorldTransform.hpp"

#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/euler_angles.hpp>

namespace Sandbox {

WorldTransform::WorldTransform(const glm::vec3 &position,
                               const glm::vec3 &rotation,
                               const glm::vec3 &scale)
    : localPosition(position), localRotation(rotation), localScale(scale),
      model() {}

glm::mat4 WorldTransform::CalcTransform(const glm::vec3 &p, const glm::vec3 &r,
                                        const glm::vec3 &s) const {
    glm::mat4 pm = glm::translate(glm::mat4(1.0f), p);
    glm::mat4 rm = glm::eulerAngleYXZ(r.y, r.x, r.z);
    glm::mat4 sm = glm::scale(glm::mat4(1.0f), s);

    return pm * rm * sm;
}

void WorldTransform::UpdateGlobal(const glm::mat4 &parent) {
    model = parent * CalcTransform(Position(), Rotation(), Scale());
}

glm::mat4 WorldTransform::ViewMatrix() const { return glm::inverse(model); }

glm::mat3 WorldTransform::NormalMatrix(const glm::mat4 &view) const {
    glm::mat4 p = glm::transpose(glm::inverse(view * model));

    return glm::mat3(p);
}

} // namespace Sandbox
