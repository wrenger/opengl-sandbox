#pragma once

#include <unordered_map>
#include <memory>
#include <string>

#include "rendering/Mesh.hpp"
#include "rendering/Shader.hpp"
#include "rendering/Texture.hpp"

namespace Sandbox {

/**
 * Shortcut for accessing assets.
 */
#ifndef ASSETS
#define ASSETS AssetDatabase::getInstance()
#endif

/**
 * @brief      Database for loading and storing assets (shaders, textures...).
 */
class AssetDatabase {
    AssetDatabase();
    ~AssetDatabase();

  public:
    // No Copies
    AssetDatabase(AssetDatabase const &) = delete;
    AssetDatabase &operator=(AssetDatabase const &) = delete;

    /**
     * @brief      Gets the instance of this singleton.
     *
     * @return     A asset database instance.
     */
    static AssetDatabase &getInstance() {
        static AssetDatabase instance;
        return instance;
    }

    static void Terminate() {
        ASSETS.shaders.clear();
        ASSETS.textures.clear();
        ASSETS.meshes.clear();
    }

    /** Stores all available shaders. */
    std::unordered_map<std::string, std::shared_ptr<Shader>> shaders;

    /** Stores all available textures. */
    std::unordered_map<std::string, std::shared_ptr<Texture>> textures;

    /** Stores all available meshes. */
    std::unordered_map<std::string, std::shared_ptr<Mesh>> meshes;
};

} // namespace Sandbox
