
#include "Mesh.hpp"

#include "Debug.hpp"
#include "IndexBuffer.hpp"
#include "VertexBuffer.hpp"
#include "VertexBufferLayout.hpp"

namespace Sandbox {

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices)
    : indexCount(indices.size()) {

    va.Bind();
    IndexBuffer ib(indices.data(), indices.size());
    va.Unbind();

    VertexBuffer vb(vertices.data(), vertices.size() * sizeof(Vertex));

    VertexBufferLayout vbl;
    vbl.PushFloat(3);
    vbl.PushFloat(3);
    vbl.PushFloat(2);

    va.AddBuffer(vb, vbl);
}

Mesh::~Mesh() {}

void Mesh::Draw() const {
    va.Bind();
    GLCall(glDrawElements(GL_TRIANGLES, (GLsizei)indexCount, GL_UNSIGNED_INT, (void *)0));
    va.Unbind();
}

// primitive shapes

MeshGen::MeshResources MeshGen::triangle() {
    std::vector<Vertex> vertices{
        Vertex{glm::vec3(-1, -1, 0), glm::vec3(0, 0, -1), glm::vec2(1, 0)},
        Vertex{glm::vec3(1, -1, 0), glm::vec3(0, 0, -1), glm::vec2(1, 1)},
        Vertex{glm::vec3(0, 1, 0), glm::vec3(0, 0, -1), glm::vec2(0, 0.5f)}};

    std::vector<unsigned int> indices{0, 1, 2};

    return MeshResources(vertices, indices);
}

MeshGen::MeshResources MeshGen::quad() {
    std::vector<Vertex> vertices{
        Vertex{glm::vec3(1, 1, 0), glm::vec3(0, 0, 1), glm::vec2(1, 0)},   // v0
        Vertex{glm::vec3(-1, 1, 0), glm::vec3(0, 0, 1), glm::vec2(0, 0)},  // v1
        Vertex{glm::vec3(-1, -1, 0), glm::vec3(0, 0, 1), glm::vec2(0, 1)}, // v2
        Vertex{glm::vec3(1, -1, 0), glm::vec3(0, 0, 1), glm::vec2(1, 1)},  // v3
    };

    std::vector<unsigned int> indices{0, 1, 2, 0, 2, 3};

    return MeshResources(vertices, indices);
}

MeshGen::MeshResources MeshGen::cube() {
    std::vector<Vertex> vertices = {
        Vertex{glm::vec3( 1,  1,  1), glm::vec3( 0,  0,  1), glm::vec2(1, 0)},      // v0
        Vertex{glm::vec3(-1,  1,  1), glm::vec3( 0,  0,  1), glm::vec2(0, 0)},      // v1
        Vertex{glm::vec3(-1, -1,  1), glm::vec3( 0,  0,  1), glm::vec2(0, 1)},      // v2
        Vertex{glm::vec3( 1, -1,  1), glm::vec3( 0,  0,  1), glm::vec2(1, 1)},      // v3

        Vertex{glm::vec3( 1,  1,  1), glm::vec3( 1,  0,  0), glm::vec2(1, 0)},      // v0
        Vertex{glm::vec3( 1, -1,  1), glm::vec3( 1,  0,  0), glm::vec2(0, 0)},      // v3
        Vertex{glm::vec3( 1, -1, -1), glm::vec3( 1,  0,  0), glm::vec2(0, 1)},      // v4
        Vertex{glm::vec3( 1,  1, -1), glm::vec3( 1,  0,  0), glm::vec2(1, 1)},      // v5

        Vertex{glm::vec3( 1,  1,  1), glm::vec3( 0,  1,  0), glm::vec2(1, 0)},      // v0
        Vertex{glm::vec3( 1,  1, -1), glm::vec3( 0,  1,  0), glm::vec2(0, 0)},      // v5
        Vertex{glm::vec3(-1,  1, -1), glm::vec3( 0,  1,  0), glm::vec2(0, 1)},      // v6
        Vertex{glm::vec3(-1,  1,  1), glm::vec3( 0,  1,  0), glm::vec2(1, 1)},      // v1

        Vertex{glm::vec3(-1,  1,  1), glm::vec3(-1,  0,  0), glm::vec2(1, 0)},      // v1
        Vertex{glm::vec3(-1,  1, -1), glm::vec3(-1,  0,  0), glm::vec2(0, 0)},      // v6
        Vertex{glm::vec3(-1, -1, -1), glm::vec3(-1,  0,  0), glm::vec2(0, 1)},      // v7
        Vertex{glm::vec3(-1, -1,  1), glm::vec3(-1,  0,  0), glm::vec2(1, 1)},      // v2

        Vertex{glm::vec3(-1, -1, -1), glm::vec3( 0, -1,  0), glm::vec2(1, 0)},      // v7
        Vertex{glm::vec3( 1, -1, -1), glm::vec3( 0, -1,  0), glm::vec2(0, 0)},      // v4
        Vertex{glm::vec3( 1, -1,  1), glm::vec3( 0, -1,  0), glm::vec2(0, 1)},      // v3
        Vertex{glm::vec3(-1, -1,  1), glm::vec3( 0, -1,  0), glm::vec2(1, 1)},      // v2

        Vertex{glm::vec3( 1, -1, -1), glm::vec3( 0,  0, -1), glm::vec2(1, 0)},      // v4
        Vertex{glm::vec3(-1, -1, -1), glm::vec3( 0,  0, -1), glm::vec2(0, 0)},      // v7
        Vertex{glm::vec3(-1,  1, -1), glm::vec3( 0,  0, -1), glm::vec2(0, 1)},      // v6
        Vertex{glm::vec3( 1,  1, -1), glm::vec3( 0,  0, -1), glm::vec2(1, 1)}       // v5
    };
    std::vector<unsigned int> indices = {
        0,  1,  2,
        0,  2,  3,
        4,  5,  6,
        4,  6,  7,
        8,  9,  10,
        8,  10, 11,
        12, 13, 14,
        12, 14, 15,
        16, 17, 18,
        16, 18, 19,
        20, 21, 22,
        20, 22, 23
    };
    return MeshResources(vertices, indices);
}

} // namespace Sandbox
