#pragma once

#include <imgui.h>
#include <memory>

#include <GLFW/glfw3.h>

#include "../../Event.hpp"
#include "../IndexBuffer.hpp"
#include "../Shader.hpp"
#include "../Texture.hpp"
#include "../VertexArray.hpp"
#include "../VertexBuffer.hpp"

namespace Sandbox {

class MainWindow;

/**
 * @brief      Module that handles the debug UI.
 */
class UI {
    /**
     * Reference to the glfw window.
     */
    MainWindow *window;

    std::shared_ptr<Shader> shader;
    std::shared_ptr<Texture> texture;

    VertexBuffer vertexBuffer;
    IndexBuffer indexBuffer;
    VertexArray vertexArray;

    GLFWcursor *mouseCursors[ImGuiMouseCursor_COUNT];

    void UpdateMousePos();
    void UpdateMouseCursor();

    void OnMouseMove(MouseMoveEvent &event);
    void OnMouseButton(MouseButtonEvent &event);
    void OnMouseScroll(MouseScrollEvent &event);
    void OnKeyboard(KeyboardEvent &event);
    void OnKeyChar(KeyCharEvent &event);
    void OnWindowResize(WindowResizeEvent &event);

    static std::shared_ptr<Texture> CreateFontTexture();

  public:
    /**
     * @brief      Initializes the UI for this window
     *
     * @param      window  The window to be used to display the ui.
     */
    UI(MainWindow *window, std::shared_ptr<Shader> shader =
                               std::make_shared<Shader>("text.shader"));
    ~UI();

    UI(UI const &) = delete;
    void operator=(UI const &) = delete;

    /**
     * @brief      Creates a new frame for the rendering.
     *
     * This has to be called before all other drawing and rendering methods.
     *
     * @param[in]  delta  Time since last frame.
     */
    void NewFrame(float delta);

    /**
     * @brief      Draws information about the OpenGL version.
     */
    void DrawInfo() const;

    /**
     * @brief      Draws the current fps.
     *
     * @param[in]  deltaTime      Time between the render calls in seconds.
     */
    void DrawFps() const;
    /**
     * @brief      Draws the current mouse cursor mode (enabled/disabled).
     */
    void DrawCursorMode() const;

    /**
     * @brief      Renders the ui.
     */
    void Render();

    /**
     * @brief      Handles input events.
     */
    void OnEvent(Event &event);
};

} // namespace Sandbox
