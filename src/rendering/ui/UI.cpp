#include "UI.hpp"

#include <glad/glad.h>

#include "../../MainWindow.hpp"
#include "../Debug.hpp"
#include "../VertexBufferLayout.hpp"

namespace Sandbox {

UI::UI(MainWindow *window, std::shared_ptr<Shader> shader)
    : window(window), shader(shader), texture(nullptr), vertexBuffer(),
      indexBuffer(), vertexArray() {

    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    // Setup back-end capabilities flags
    ImGuiIO &io = ImGui::GetIO();
    io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;
    io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;
    io.BackendRendererName = "sandbox";

    // Keyboard mapping. ImGui will use those indices to peek into the
    // io.KeysDown[] array.
    io.KeyMap[ImGuiKey_Tab] = GLFW_KEY_TAB;
    io.KeyMap[ImGuiKey_LeftArrow] = GLFW_KEY_LEFT;
    io.KeyMap[ImGuiKey_RightArrow] = GLFW_KEY_RIGHT;
    io.KeyMap[ImGuiKey_UpArrow] = GLFW_KEY_UP;
    io.KeyMap[ImGuiKey_DownArrow] = GLFW_KEY_DOWN;
    io.KeyMap[ImGuiKey_PageUp] = GLFW_KEY_PAGE_UP;
    io.KeyMap[ImGuiKey_PageDown] = GLFW_KEY_PAGE_DOWN;
    io.KeyMap[ImGuiKey_Home] = GLFW_KEY_HOME;
    io.KeyMap[ImGuiKey_End] = GLFW_KEY_END;
    io.KeyMap[ImGuiKey_Insert] = GLFW_KEY_INSERT;
    io.KeyMap[ImGuiKey_Delete] = GLFW_KEY_DELETE;
    io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
    io.KeyMap[ImGuiKey_Space] = GLFW_KEY_SPACE;
    io.KeyMap[ImGuiKey_Enter] = GLFW_KEY_ENTER;
    io.KeyMap[ImGuiKey_Escape] = GLFW_KEY_ESCAPE;
    io.KeyMap[ImGuiKey_A] = GLFW_KEY_A;
    io.KeyMap[ImGuiKey_C] = GLFW_KEY_C;
    io.KeyMap[ImGuiKey_V] = GLFW_KEY_V;
    io.KeyMap[ImGuiKey_X] = GLFW_KEY_X;
    io.KeyMap[ImGuiKey_Y] = GLFW_KEY_Y;
    io.KeyMap[ImGuiKey_Z] = GLFW_KEY_Z;

    io.ClipboardUserData = window;
    io.SetClipboardTextFn = [](void *data, const char *text) {
        static_cast<MainWindow *>(data)->SetClipboardText(text);
    };
    io.GetClipboardTextFn = [](void *data) {
        return static_cast<MainWindow *>(data)->GetClipboardText();
    };

    glm::vec2 size = window->WindowSize();
    io.DisplaySize = ImVec2(size.x, size.y);

    glm::vec2 scale =
        size.x > 0 && size.y > 0 ? window->ViewportSize() / size : glm::vec2();
    io.DisplayFramebufferScale = ImVec2(scale.x, scale.y);

    mouseCursors[ImGuiMouseCursor_Arrow] =
        glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
    mouseCursors[ImGuiMouseCursor_TextInput] =
        glfwCreateStandardCursor(GLFW_IBEAM_CURSOR);
    mouseCursors[ImGuiMouseCursor_ResizeAll] = glfwCreateStandardCursor(
        GLFW_ARROW_CURSOR); // FIXME: GLFW doesn't have this.
    mouseCursors[ImGuiMouseCursor_ResizeNS] =
        glfwCreateStandardCursor(GLFW_VRESIZE_CURSOR);
    mouseCursors[ImGuiMouseCursor_ResizeEW] =
        glfwCreateStandardCursor(GLFW_HRESIZE_CURSOR);
    mouseCursors[ImGuiMouseCursor_ResizeNESW] = glfwCreateStandardCursor(
        GLFW_ARROW_CURSOR); // FIXME: GLFW doesn't have this.
    mouseCursors[ImGuiMouseCursor_ResizeNWSE] = glfwCreateStandardCursor(
        GLFW_ARROW_CURSOR); // FIXME: GLFW doesn't have this.
    mouseCursors[ImGuiMouseCursor_Hand] =
        glfwCreateStandardCursor(GLFW_HAND_CURSOR);

    // Initialize rendering
    texture = CreateFontTexture();
    io.Fonts->TexID = (ImTextureID)(intptr_t)texture->Id();

    VertexBufferLayout layout;
    layout.PushFloat(2);
    layout.PushFloat(2);
    layout.PushByte(4);
    vertexArray.AddBuffer(vertexBuffer, layout);
}

UI::~UI() {
    for (GLFWcursor *cursor : mouseCursors) {
        glfwDestroyCursor(cursor);
    }

    ImGui::DestroyContext();
}

std::shared_ptr<Texture> UI::CreateFontTexture() {
    // Build texture atlas
    ImGuiIO &io = ImGui::GetIO();
    unsigned char *pixels;
    int width, height;
    // Load as RGBA 32-bits (75% of the memory is wasted, but default font is so
    // small) because it is more likely to be compatible with user's existing
    // shaders. If your ImTextureId represent a higher-level concept than just a
    // GL texture id, consider calling GetTexDataAsAlpha8() instead to save on
    // GPU memory.
    io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);
    Image image{pixels, width, height, 1};

    return std::make_shared<Texture>(image, true);
}

void UI::NewFrame(float delta) {
    ImGuiIO &io = ImGui::GetIO();
    IM_ASSERT(io.Fonts->IsBuilt());
    io.DeltaTime = delta;

    if (!(io.ConfigFlags & ImGuiConfigFlags_NoMouseCursorChange) &&
        window->IsCursorEnabled()) {
        UpdateMousePos();
        UpdateMouseCursor();
    }

    // TODO: Map gamepad navigation.

    ImGui::NewFrame();
}

void UI::UpdateMousePos() {
    // Update buttons
    ImGuiIO &io = ImGui::GetIO();

    // Update mouse position
    if (window->IsFocused() && io.WantSetMousePos) {
        window->SetCursor(glm::vec2(io.MousePos.x, io.MousePos.y));
    }
}

void UI::UpdateMouseCursor() {
    ImGuiIO &io = ImGui::GetIO();

    ImGuiMouseCursor imgui_cursor = ImGui::GetMouseCursor();
    if (imgui_cursor == ImGuiMouseCursor_None || io.MouseDrawCursor) {
        // Hide OS mouse cursor if imgui is drawing it or if it wants no cursor
        glfwSetInputMode(window->Glfw(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    } else {
        glfwSetCursor(window->Glfw(), mouseCursors[imgui_cursor]);
        glfwSetInputMode(window->Glfw(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
}

void UI::DrawInfo() const { ImGui::Text("OpenGL %s", glGetString(GL_VERSION)); }

void UI::DrawFps() const {
    ImGuiIO &io = ImGui::GetIO();
    ImGui::Text("%.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate,
                io.Framerate);
}

void UI::DrawCursorMode() const {
    ImGui::Value("Cursor enabled", window->IsCursorEnabled());
}

void UI::Render() {
    ImGui::Render();
    ImDrawData *draw_data = ImGui::GetDrawData();

    int fb_width = draw_data->DisplaySize.x * draw_data->FramebufferScale.x;
    int fb_height = draw_data->DisplaySize.y * draw_data->FramebufferScale.y;
    if (fb_width <= 0 || fb_height <= 0)
        return;

    // Backup GL state
    GLint last_polygon_mode[2];
    glGetIntegerv(GL_POLYGON_MODE, last_polygon_mode);
    GLint last_scissor_box[4];
    glGetIntegerv(GL_SCISSOR_BOX, last_scissor_box);
    GLenum last_blend_src_rgb;
    glGetIntegerv(GL_BLEND_SRC_RGB, (GLint *)&last_blend_src_rgb);
    GLenum last_blend_dst_rgb;
    glGetIntegerv(GL_BLEND_DST_RGB, (GLint *)&last_blend_dst_rgb);
    GLenum last_blend_src_alpha;
    glGetIntegerv(GL_BLEND_SRC_ALPHA, (GLint *)&last_blend_src_alpha);
    GLenum last_blend_dst_alpha;
    glGetIntegerv(GL_BLEND_DST_ALPHA, (GLint *)&last_blend_dst_alpha);
    GLenum last_blend_equation_rgb;
    glGetIntegerv(GL_BLEND_EQUATION_RGB, (GLint *)&last_blend_equation_rgb);
    GLenum last_blend_equation_alpha;
    glGetIntegerv(GL_BLEND_EQUATION_ALPHA, (GLint *)&last_blend_equation_alpha);
    GLboolean last_enable_blend = glIsEnabled(GL_BLEND);
    GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
    GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
    GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);

    // Setup
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glm::mat4 ortho_projection =
        glm::ortho(draw_data->DisplayPos.x,
                   draw_data->DisplayPos.x + draw_data->DisplaySize.x,
                   draw_data->DisplayPos.y + draw_data->DisplaySize.y,
                   draw_data->DisplayPos.y);

    texture->Bind();
    shader->Bind(ortho_projection);

    vertexArray.Bind();
    vertexBuffer.Bind();
    indexBuffer.Bind();

    // Render command lists
    for (int n = 0; n < draw_data->CmdListsCount; n++) {
        const ImDrawList *cmd_list = draw_data->CmdLists[n];
        size_t idx_buffer_offset = 0;

        vertexBuffer.UpdateData(cmd_list->VtxBuffer.Data,
                                cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));

        indexBuffer.UpdateData((const unsigned short *)cmd_list->IdxBuffer.Data,
                               cmd_list->IdxBuffer.Size);

        for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++) {
            const ImDrawCmd *pcmd = &cmd_list->CmdBuffer[cmd_i];
            if (pcmd->UserCallback) {
                // User callback (registered via ImDrawList::AddCallback)
                pcmd->UserCallback(cmd_list, pcmd);
            } else {
                // Will project scissor/clipping rectangles into framebuffer
                // space
                // (0,0) unless using multi-viewports
                ImVec2 clip_off = draw_data->DisplayPos;
                // (1,1) unless using retina display which are often (2,2)
                ImVec2 clip_scale = draw_data->FramebufferScale;
                // Project scissor/clipping rectangles into framebuffer space
                int clip_x = (pcmd->ClipRect.x - clip_off.x) * clip_scale.x;
                int clip_y = (pcmd->ClipRect.y - clip_off.y) * clip_scale.y;
                int clip_z = (pcmd->ClipRect.z - clip_off.x) * clip_scale.x;
                int clip_w = (pcmd->ClipRect.w - clip_off.y) * clip_scale.y;

                if (clip_x < fb_width && clip_y < fb_height && clip_z >= 0 &&
                    clip_w >= 0) {
                    // Apply scissor/clipping rectangle
                    GLCall(glScissor(clip_x, fb_height - clip_w,
                                     clip_z - clip_x, clip_w - clip_y));

                    // Bind texture, Draw
                    GLCall(glDrawElements(
                        GL_TRIANGLES, (GLsizei)pcmd->ElemCount,
                        GL_UNSIGNED_SHORT, (void *)idx_buffer_offset));
                }
            }
            idx_buffer_offset += pcmd->ElemCount * sizeof(ImDrawIdx);
        }
    }

    shader->Unbind();
    texture->Unbind();

    vertexArray.Unbind();
    vertexBuffer.Unbind();
    indexBuffer.Unbind();

    // Restore modified GL state
    glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
    glBlendFuncSeparate(last_blend_src_rgb, last_blend_dst_rgb,
                        last_blend_src_alpha, last_blend_dst_alpha);
    if (!last_enable_blend)
        glDisable(GL_BLEND);
    if (last_enable_cull_face)
        glEnable(GL_CULL_FACE);
    if (last_enable_depth_test)
        glEnable(GL_DEPTH_TEST);
    if (!last_enable_scissor_test)
        glDisable(GL_SCISSOR_TEST);
    glPolygonMode(GL_FRONT_AND_BACK, (GLenum)last_polygon_mode[0]);
    glScissor(last_scissor_box[0], last_scissor_box[1],
              (GLsizei)last_scissor_box[2], (GLsizei)last_scissor_box[3]);
}

void UI::OnEvent(Event &event) {
    switch (event.GetType()) {
    case EventType::MouseMove:
        OnMouseMove((MouseMoveEvent &)event);
        break;
    case EventType::MouseButton:
        OnMouseButton((MouseButtonEvent &)event);
        break;
    case EventType::MouseScroll:
        OnMouseScroll((MouseScrollEvent &)event);
        break;
    case EventType::Keyboard:
        OnKeyboard((KeyboardEvent &)event);
        break;
    case EventType::KeyChar:
        OnKeyChar((KeyCharEvent &)event);
        break;
    case EventType::WindowResize:
        OnWindowResize((WindowResizeEvent &)event);
        break;
    default:
        break;
    }
}

void UI::OnMouseMove(MouseMoveEvent &event) {
    ImGuiIO &io = ImGui::GetIO();
    io.MousePos = ImVec2(event.GetPosition().x, event.GetPosition().y);
}

void UI::OnMouseButton(MouseButtonEvent &event) {
    ImGuiIO &io = ImGui::GetIO();
    if (event.GetButton() >= 0 &&
        event.GetButton() < IM_ARRAYSIZE(io.MouseDown)) {
        io.MouseDown[event.GetButton()] =
            event.GetAction() == KeyAction::KeyPressed;
    }
}

void UI::OnMouseScroll(MouseScrollEvent &event) {
    ImGuiIO &io = ImGui::GetIO();
    io.MouseWheelH += event.GetOffset().x;
    io.MouseWheel += event.GetOffset().y;
}

void UI::OnKeyboard(KeyboardEvent &event) {
    ImGuiIO &io = ImGui::GetIO();
    if (event.GetAction() == KeyAction::KeyPressed) {
        io.KeysDown[event.GetKeyCode()] = true;
    } else if (event.GetAction() == KeyAction::KeyReleased) {
        io.KeysDown[event.GetKeyCode()] = false;
    }

    io.KeyCtrl = io.KeysDown[GLFW_KEY_LEFT_CONTROL] ||
                 io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
    io.KeyShift =
        io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
    io.KeyAlt =
        io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
    io.KeySuper =
        io.KeysDown[GLFW_KEY_LEFT_SUPER] || io.KeysDown[GLFW_KEY_RIGHT_SUPER];
}

void UI::OnKeyChar(KeyCharEvent &event) {
    ImGuiIO &io = ImGui::GetIO();
    if (event.GetCharacter() > 0 && event.GetCharacter() < 0x10000) {
        io.AddInputCharacter((ImWchar)event.GetCharacter());
    }
}

void UI::OnWindowResize(WindowResizeEvent &event) {
    ImGuiIO &io = ImGui::GetIO();
    glm::vec2 size(event.GetWidth(), event.GetHeight());
    io.DisplaySize = ImVec2(size.x, size.y);

    glm::vec2 display_size =
        size.x > 0 && size.y > 0 ? window->ViewportSize() / size : glm::vec2();
    io.DisplayFramebufferScale = ImVec2(display_size.x, display_size.y);
}

} // namespace Sandbox
