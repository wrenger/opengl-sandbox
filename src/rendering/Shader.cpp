
#include "Shader.hpp"

#include <fstream>
#include <iostream>
#include <sstream>

#include "Debug.hpp"

namespace Sandbox {

Shader::ShaderSource Shader::parseShader(const std::string &filename) {
    std::ifstream stream(PROJECT_SOURCE_DIR SHADER_PATH + filename);

    enum class ShaderType : int { NONE = -1, VERTEX = 0, FRAGMENT = 1 };
    ShaderType type = ShaderType::NONE;

    std::stringstream ss[2];
    std::string line;
    while (getline(stream, line)) {
        if (line.find("#shader") != std::string::npos) {
            if (line.find("vertex") != std::string::npos)
                type = ShaderType::VERTEX;
            else if (line.find("fragment") != std::string::npos)
                type = ShaderType::FRAGMENT;
        } else {
            if (type != ShaderType::NONE)
                ss[(int)type] << line << "\n";
        }
    }

    return ShaderSource{ss[(int)ShaderType::VERTEX].str(),
                        ss[(int)ShaderType::FRAGMENT].str()};
}

Shader::Shader(const std::string &filename) {
    ShaderSource source = parseShader(filename);

    unsigned int vertShader = compileShader(GL_VERTEX_SHADER, source.vertex);
    unsigned int fragShader =
        compileShader(GL_FRAGMENT_SHADER, source.fragment);

    program = createProgram(vertShader, fragShader);

    // Caching uniform locations.
    int count;
    GLCall(glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &count));

    const unsigned int nameSize = 32;
    char name[nameSize];
    int len;
    int size;
    unsigned int type;
    for (int i = 0; i < count; ++i) {
        GLCall(glGetActiveUniform(program, (GLuint)i, nameSize, &len, &size,
                                  &type, name));
        uniform_locations[name] = i;
    }
}

Shader::~Shader() { GLCall(glDeleteProgram(program)); }

unsigned int Shader::compileShader(unsigned int type, const std::string &raw) {
    GLCall(unsigned int shader = glCreateShader(type));
    const char *source = raw.c_str();
    GLCall(glShaderSource(shader, 1, &source, nullptr));
    GLCall(glCompileShader(shader));

    int success;
    GLCall(glGetShaderiv(shader, GL_COMPILE_STATUS, &success));
    if (!success) {
        int length;
        GLCall(glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length));
        char message[length];
        GLCall(glGetShaderInfoLog(shader, length, &length, message));
        std::cerr << message << std::endl;
    }

    return shader;
}

unsigned int Shader::createProgram(unsigned int vert, unsigned int frag) {
    GLCall(unsigned int shaderProgram = glCreateProgram());
    GLCall(glAttachShader(shaderProgram, vert));
    GLCall(glAttachShader(shaderProgram, frag));
    GLCall(glLinkProgram(shaderProgram));

    int success;
    GLCall(glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success));
    if (!success) {
        int length;
        GLCall(glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &length));
        char message[length];
        GLCall(glGetProgramInfoLog(shaderProgram, length, nullptr, message));
        std::cerr << message << std::endl;
    }

    GLCall(glDeleteShader(vert));
    GLCall(glDeleteShader(frag));

    return shaderProgram;
}

void Shader::Bind() const { GLCall(glUseProgram(program)); }

void Shader::Bind(const glm::mat4 &projection) const {
    Bind();
    Uniform("u_projectionMatrix", projection);
}

void Shader::Bind(const glm::mat4 &projection, const glm::mat4 &view) const {
    Bind(projection);
    Uniform("u_viewMatrix", view);
}

void Shader::Bind(const glm::mat4 &projection, const glm::mat4 &view,
                  const glm::mat4 &model) const {
    Bind(projection, view);
    Uniform("u_modelMatrix", model);
}

void Shader::Bind(const glm::mat4 &projection, const glm::mat4 &view,
                  const glm::mat4 &model, const glm::mat3 &normal) const {
    Bind(projection, view, model);
    Uniform("u_normalMatrix", normal);
}

void Shader::Unbind() const { GLCall(glUseProgram(0)); }

void Shader::Diffuse(const glm::vec3 &dir, const glm::vec3 &color,
                     float lightImpact) const {
    Uniform("u_lightPos", dir);
    Uniform("u_lightColor", color);
    Uniform("u_lightImpact", lightImpact);
}

void Shader::Uniform(const std::string &location, int val) const {
    GLCall(glUniform1i(uniformId(location), val));
}

void Shader::Uniform(const std::string &location, float val) const {
    GLCall(glUniform1f(uniformId(location), val));
}

void Shader::Uniform(const std::string &location, const glm::vec2 &val) const {
    GLCall(glUniform2fv(uniformId(location), 1, glm::value_ptr(val)));
}

void Shader::Uniform(const std::string &location, const glm::vec3 &val) const {
    GLCall(glUniform3fv(uniformId(location), 1, glm::value_ptr(val)));
}

void Shader::Uniform(const std::string &location, const glm::vec4 &val) const {
    GLCall(glUniform4fv(uniformId(location), 1, glm::value_ptr(val)));
}

void Shader::Uniform(const std::string &location, const glm::mat3 &val) const {
    GLCall(glUniformMatrix3fv(uniformId(location), 1, GL_FALSE,
                              glm::value_ptr(val)));
}

void Shader::Uniform(const std::string &location, const glm::mat4 &val) const {
    GLCall(glUniformMatrix4fv(uniformId(location), 1, GL_FALSE,
                              glm::value_ptr(val)));
}

} // namespace Sandbox
