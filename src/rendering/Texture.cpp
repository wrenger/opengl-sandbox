#define STB_IMAGE_IMPLEMENTATION

#include "Texture.hpp"

#include <iostream>
#include <stb_image.h>

#include "Debug.hpp"

namespace Sandbox {

void Texture::init(Image &image, bool alpha) {
    GLCall(glGenTextures(1, &id));
    GLCall(glBindTexture(GL_TEXTURE_2D, id));
    GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
    GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));
    GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                           GL_LINEAR_MIPMAP_NEAREST));
    GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));

    unsigned int format = alpha ? GL_RGBA : GL_RGB;
    GLCall(glTexImage2D(GL_TEXTURE_2D, 0, format, image.width, image.height,
                        0, format, GL_UNSIGNED_BYTE, image.data));
    GLCall(glGenerateMipmap(GL_TEXTURE_2D));
}

Texture::Texture(Image &image, bool alpha) {
    init(image, alpha);
}

Texture::Texture(std::string filepath, bool alpha) {
    Image image = loadImage(filepath);
    if (image.data != nullptr) {
        init(image, alpha);
        stbi_image_free(image.data);
    } else {
        std::cerr << "Texture source not found: " << filepath << std::endl;
    }
}

Texture::~Texture() { GLCall(glDeleteTextures(1, &id)); }

Image Texture::loadImage(std::string filepath) {
    std::string path = PROJECT_SOURCE_DIR TEXTURE_PATH + filepath;
    int width, height, channels;
    unsigned char *data =
        stbi_load(path.c_str(), &width, &height, &channels, 0);
    return Image{data, width, height, channels};
}

void Texture::Bind() const { GLCall(glBindTexture(GL_TEXTURE_2D, id)); }

void Texture::Unbind() const { GLCall(glBindTexture(GL_TEXTURE_2D, 0)); }

} // namespace Sandbox
