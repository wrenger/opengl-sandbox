#include "Debug.hpp"

#include <iomanip>
#include <iostream>

namespace Sandbox {

void GLClearError() {
    while (glGetError() != GL_NO_ERROR)
        ;
}

bool GLLogError(const char *function, const char *file, unsigned int line) {
    bool found = false;
    while (GLenum error = glGetError()) {
        std::cerr << "[ OpenGL Error ] (0x" << std::hex << error
                  << "): " << function << " " << file << ":" << line
                  << std::endl;
        found = true;
    }
    return found;
}

} // namespace Sandbox
