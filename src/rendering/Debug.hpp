#pragma once

#include <glad/glad.h> // keep

#include <cassert> // keep

#ifndef PROJECT_SOURCE_DIR
#define PROJECT_SOURCE_DIR ""
#endif

namespace Sandbox {

#if 1
#define GLCall(x)                                                              \
    GLClearError();                                                            \
    x;                                                                         \
    assert(!GLLogError(#x, __FILE__, __LINE__))

/**
 * @brief      Clears all OpenGL Errors.
 */
void GLClearError();

/**
 * @brief      Logs all OpenGL Errors and returns if at least one occurs.
 *
 * @param[in]  function  The OpenGL function that is called.
 * @param[in]  file      The file which contains the function.
 * @param[in]  line      The line of this function.
 *
 * @return     If at least one error occurs.
 */
bool GLLogError(const char *function, const char *file, unsigned int line);

#else
#define GLCall(x) x;
#endif

} // namespace Sandbox
