#include "IndexBuffer.hpp"

#include <glad/glad.h>

#include "Debug.hpp"

namespace Sandbox {

IndexBuffer::IndexBuffer() { GLCall(glGenBuffers(1, &rendererID)); }

IndexBuffer::IndexBuffer(const unsigned int *data, unsigned int count)
    : IndexBuffer() {
    UpdateData(data, count);
}

IndexBuffer::IndexBuffer(const unsigned short *data, unsigned int count)
    : IndexBuffer() {
    UpdateData(data, count);
}

IndexBuffer::~IndexBuffer() { GLCall(glDeleteBuffers(1, &rendererID)); }

void IndexBuffer::UpdateData(const unsigned int *data, unsigned int count) {
    Bind();
    GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int),
                        data, GL_STATIC_DRAW));
}

void IndexBuffer::UpdateData(const unsigned short *data, unsigned int count) {
    Bind();
    GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned short),
                        data, GL_STATIC_DRAW));
}

void IndexBuffer::Bind() const {
    GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererID));
}

void IndexBuffer::Unbind() const {
    GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
}

} // namespace Sandbox
