#include "VertexArray.hpp"

#include <glad/glad.h>

#include "Debug.hpp"

namespace Sandbox {

VertexArray::VertexArray() { GLCall(glGenVertexArrays(1, &rendererID)); }

VertexArray::~VertexArray() { GLCall(glDeleteVertexArrays(1, &rendererID)); }

void VertexArray::AddBuffer(const VertexBuffer &vb,
                            const VertexBufferLayout &layout) {
    Bind();
    vb.Bind();

    unsigned long offset = 0;
    const auto &elements = layout.Elements();
    for (unsigned int i = 0; i < elements.size(); i++) {
        const VertexBufferElement &element = elements[i];
        GLCall(glEnableVertexAttribArray(i));
        GLCall(glVertexAttribPointer(i, element.count, element.type,
                                     element.normalized, layout.Stride(),
                                     (const void *)offset));
        offset += element.Size();
    }

    vb.Unbind();
    Unbind();
}

void VertexArray::Bind() const { GLCall(glBindVertexArray(rendererID)); }

void VertexArray::Unbind() const { GLCall(glBindVertexArray(0)); }

} // namespace Sandbox
