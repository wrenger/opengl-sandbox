#pragma once

#include <vector>

#include <glad/glad.h>

namespace Sandbox {

struct VertexBufferElement {
    unsigned int type;
    unsigned int count;
    unsigned char normalized;

    inline unsigned int Size() const {
        switch (type) {
            case GL_FLOAT:          return sizeof(GLfloat) * count;
            case GL_UNSIGNED_INT:   return sizeof(GLuint) * count;
            case GL_UNSIGNED_BYTE:  return sizeof(GLubyte) * count;
        }
        return 0;
    }
};

class VertexBufferLayout {
    std::vector<VertexBufferElement> elements;
    unsigned int stride;

  public:
    VertexBufferLayout() : stride(0) {}
    ~VertexBufferLayout() {}

    void PushFloat(unsigned int count);

    void PushUint(unsigned int count);

    void PushByte(unsigned int count);

    inline std::vector<VertexBufferElement> const &Elements() const {
        return elements;
    }

    inline unsigned int Stride() const { return stride; }
};

} // namespace Sandbox
