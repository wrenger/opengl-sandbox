#pragma once

#include <memory>

#include "scenegraph/Node.hpp"
#include "scenegraph/nodes/Camera.hpp"

namespace Sandbox {

class MainWindow;

/**
 * @brief  The scene represents the three dimensional world and
 *         holds the root of the scene graph.
 */
class Scene {
    MainWindow &window;

    std::shared_ptr<Node> root;
    std::shared_ptr<Camera> camera;

    /**
     * @brief Setup the window callbacks for the main loop and the event system
     */
    void initCallbacks();

    void OnKey(KeyboardEvent &event);
    void OnMouseButton(MouseButtonEvent &event);

  public:
    /**
     * @brief      Creates a new scene inside this window
     *
     * @param[in]  window  The window of this scene
     */
    explicit Scene(MainWindow &window);

    /**
     * @brief      Destroys the object.
     */
    ~Scene();

    Scene(Scene const &) = delete;
    void operator=(Scene const &) = delete;

    /**
     * @brief      Update callback which is called every tick.
     *
     * @param[in]  deltatime  The time until the last update call in seconds.
     */
    void Update(float deltatime);

    /**
     * @brief      Render callback which is called every tick.
     *
     * @param[in]  deltatime  The time until the last render call in seconds.
     */
    void Render(float deltatime) const;

    /**
     * @brief     Handles events.
     *
     * @param[in] event Event Class.
     */
    void OnEvent(Event &event);
};

} // namespace Sandbox
