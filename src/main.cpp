#include "AssetDatabase.hpp"
#include "MainWindow.hpp"
#include "Scene.hpp"

using namespace Sandbox;

int main(int argc, char const* argv[]) {

    MainWindow::Initialize();

    {
        // Create a new window
        MainWindow win;
        // Create a new scene for this window
        Scene scene(win);
        // Display the window
        win.showAndWait();
    }

    // Free assets before destroying the gl context
    AssetDatabase::Terminate();
    MainWindow::Terminate();

    return EXIT_SUCCESS;
}
