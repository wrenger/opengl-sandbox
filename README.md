# OpenGL Sandbox

A simple boilerplate for learning and experimenting with modern OpenGL.

This project is build with C++14 and OpenGL 4.1.

## Getting Started

The only dependency for this project is [cmake](https://cmake.org/), a tool for generating platform-specific build definitions (makefiles).

To clone the complete repository containing all submodules make sure to use the `--recursive` flag:

```bash
git clone --recursive [clone-url]
```

If you forget this flag, run `git submodule update --init` afterwards.

Now generate the build definitions (makefile and project files) for your platform. Most IDEs already support cmake like Visual Studio on Windows.

```bash
# macos/linux/mingw:
# create the build directory
mkdir ./build && cd ./build
# generate makefile and project files
cmake ..
```

For more information see [cmake](https://cmake.org/documentation/).

> Every time when you create or remove source files, the makefile and project files must be regenerated, before you compile the project.

Now after generating the makefile and project files, the project source itself has to be compiled. The generated makefile for this is stored directly in the [build](/build) directory.

```bash
# macos/linux/mingw:
# go to the build directory if you aren't already there
cd ./build
# compile the project source and its dependencies
make
# run the application
./opengl_sandbox/opengl_sandbox
```

## Documentation

The source code of this project is documented with Doxygen comments.

All dependencies of this project are included as git submodules that are stored in the [external](/external) directory. Most of them contain their own documentation and examples.

Library | Functionality
--|--
[glad](https://github.com/Dav1dde/glad/tree/master) | OpenGl Function Loader
[glfw](http://www.glfw.org/) | Cross-Platform Window and Input
[glm](https://glm.g-truc.net/) | OpenGl Math
[imgui](https://github.com/ocornut/imgui) | Simple Debug GUI
[stb](https://github.com/nothings/stb) | Texture Loading

New libraries can easily be added as submodules, too.

See [git submodule](https://git-scm.com/docs/git-submodule) for more information.

To learn more about OpenGL I would recommend the [OpenGL tutorial series](https://www.youtube.com/playlist?list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2) by [The Cherno Project](https://www.youtube.com/user/TheChernoProject).
