#shader vertex
#version 330 core

uniform mat4 u_projectionMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_modelMatrix;
uniform mat3 u_normalMatrix;

layout(location = 0) in vec3 i_position;
layout(location = 1) in vec3 i_normal;
layout(location = 2) in vec2 i_uv;

out vec2 v_textureCoord;

void main() {
    gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vec4(i_position, 1.0);
    v_textureCoord = i_uv;
}

#shader fragment
#version 330 core

uniform sampler2D tex0;

in vec2 v_textureCoord;

layout(location = 0) out vec4 outputColor;

void main() {
    outputColor = texture(tex0, v_textureCoord);
}
