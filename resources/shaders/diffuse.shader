#shader vertex
#version 330 core

uniform mat4 u_projectionMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_modelMatrix;
uniform mat3 u_normalMatrix;

uniform vec3 u_lightPos;
uniform vec3 u_lightColor;
uniform float u_lightImpact;

layout(location = 0) in vec3 i_position;
layout(location = 1) in vec3 i_normal;
layout(location = 2) in vec2 i_uv;

out vec2 v_textureCoord;
out vec4 v_light;

void main() {
    gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vec4(i_position, 1.0);
    v_textureCoord = i_uv;

    vec3 nnormal = normalize(u_normalMatrix * i_normal);
    vec3 lightDir = normalize((u_viewMatrix * vec4(u_lightPos, 0)).xyz);

    float lambert = 1 - u_lightImpact + dot(nnormal, lightDir) * u_lightImpact;
    v_light = vec4(lambert * u_lightColor, 1);
}

#shader fragment
#version 330 core

uniform sampler2D tex0;

in vec2 v_textureCoord;
in vec4 v_light;

layout(location = 0) out vec4 outputColor;

void main() {
    outputColor = texture(tex0, v_textureCoord) * v_light;
}
