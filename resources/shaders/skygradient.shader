#shader vertex
#version 330 core

uniform mat4 u_projectionMatrix;
uniform mat4 u_viewMatrix;

layout(location = 0) in vec3 i_position;

out vec3 v_position;

void main() {
    mat4 view = u_viewMatrix;
    view[3][0] = 0.0f;
    view[3][1] = 0.0f;
    view[3][2] = 0.0f;

    gl_Position = u_projectionMatrix * view * vec4(i_position, 1.0f);
    v_position = i_position;
}


#shader fragment
#version 330 core

uniform vec4 u_skyHorizon;
uniform vec4 u_skyTop;

in vec3 v_position;

layout(location = 0) out vec4 outputColor;

void main() {
     vec3 sphere = normalize(v_position);
     float t = sphere.y;
     outputColor = mix(u_skyHorizon, u_skyTop, t * 0.75f + 0.5f);
}
